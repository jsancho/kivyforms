from setuptools import setup
import os

def get_version():
    with open('VERSION') as fd:
        return fd.read().strip()

def get_long_description():
    with open(os.path.join(
        os.path.dirname(os.path.abspath(__file__)), 'README.md'
    ), encoding='utf8') as fp:
        return fp.read()


setup(
    name='kivyforms',
    version=get_version(),
    description='Tools for making forms easy',
    long_description=get_long_description(),
    long_description_content_type='text/markdown',
    author='Javier Sancho',
    url='https://git.jsancho.org/kivyforms',
    license='MIT',
    packages=['kivyforms'],
    install_requires=['kivy']
)
